const createError = require('http-errors');
const express = require('express');
const path = require('path');
const debug = require('debug')('testexpress2:server');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const http = require('http');

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server)

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

let indexRouter = require('./routes');
let adminRouter = require('./routes/admin');

app.use('/', indexRouter);
app.use('/admin', adminRouter);

// Set constants
const CANVAS_HEIGHT = 700
const CANVAS_WIDTH = 1000
const PIXEL_SIZE = 10

const CANVAS_ROWS = CANVAS_HEIGHT / PIXEL_SIZE;
const CANVAS_COLS = CANVAS_WIDTH / PIXEL_SIZE;

let connections = 0;

let canvas = newCanvas();
let coolDown = 5;

let status = true;

let interval = null;
let randomInterval = 10000;
let intervalStatus = false;

const adminPassword = "Tz58!cK2@b7U5Eto8Mq0";

io.on("connection", socket => {
    connections++;
    io.emit('online_counter', {count: connections});

    socket.emit("update_canvas", canvas);
    socket.emit('cooldown', {cooldown: coolDown});
    socket.emit('random_pixel_interval', {interval: randomInterval});
    socket.emit('random_pixel_status', {status: intervalStatus});
    socket.emit('game_status', {status: status});

    socket.on("admin-code", data => {
        socket.emit("admin-check", {
            check: data.code === adminPassword
        })
    })
    // Update the canvas and send it to all connected clients
    socket.on("place_pixel", data => {
        // Update the canvas
        if (data.y < CANVAS_ROWS && data.y >= 0 && data.x < CANVAS_COLS && data.x >= 0) {
            canvas[data.y][data.x] = data.color;
            // Send the new pixel to all connected clients
            io.emit("update_pixel", data);
        }
    });

    socket.on('cooldown', data => {
        coolDown = data.cooldown;
        // Set the new cooldown to all connected clients
        io.emit('cooldown', data);
    });

    // Update the online counter on a client disconnecting
    socket.on('disconnect', function () {
        connections--;
        io.emit('online_counter', {count: connections});
    });

    // Clear the interval for random pixel creation
    socket.on('random_pixel_stop', data => {
        if (interval === null) return;
        clearInterval(interval);
        interval = null;
        intervalStatus = false;
        io.emit('random_pixel_status', {status: false});
    });

    // Start the interval for random pixel creation
    socket.on('random_pixel_start', data => {
        if (interval !== null) return;
        interval = setInterval(randomPixel, randomInterval);
        intervalStatus = true;
        io.emit('random_pixel_status', {status: true});
    });

    // Change the interval for random pixel creation
    socket.on('random_pixel_set_interval', data => {
        randomInterval = data.interval;
        // If the interval is already running, reset the interval with the new time in ms
        if (interval !== null) {
            clearInterval(interval);
            interval = setInterval(randomPixel, randomInterval);
        }
        io.emit('random_pixel_interval', {interval: randomInterval});
    });

    // Update the game status
    socket.on('update_game_status', data => {
        status = data.status;
        if (!status && interval !== null) {
            clearInterval(interval)
            interval = null;
        } else if (status && interval === null && intervalStatus) {
            interval = setInterval(randomPixel, randomInterval);
        }
        io.emit('game_status', data);
    });

    // Reset the canvas and all values
    socket.on('reset', data => {
        canvas = newCanvas();
        clearInterval(interval);
        interval = null;
        randomInterval = 10000;
        intervalStatus = false;
        io.emit('update_canvas', canvas);
        io.emit('random_pixel_status', {status: false});
        io.emit('random_pixel_interval', {interval: randomInterval});
    });
});

/**
 * Finds a random coordinate and colors it with a random color
 */
function randomPixel() {
    const x = Math.floor(Math.random() * Math.floor(CANVAS_COLS));
    const y = Math.floor(Math.random() * Math.floor(CANVAS_ROWS));

    const colors = [
        '#ffffff',
        '#e4e4e4',
        '#888888',
        '#222222',
        '#ffa7d1',
        '#e50000',
        '#e59500',
        '#a06b42',
        '#e5da00',
        '#94e044',
        '#02be01',
        '#00d3dd',
        '#0083c7',
        '#0000ea',
        '#cf6ee4',
        '#820080',
    ]

    const color = colors[Math.floor(Math.random() * colors.length)];
    if (canvas[y][x] === color) return;
    canvas[y][x] = color;
    io.emit('update_pixel', {x: x, y: y, color: color});
}

/**
 * Reset the canvas to all white
 */
function newCanvas() {
    let temp = [];
    for (let y = 0; y < CANVAS_HEIGHT / PIXEL_SIZE; y++) {
        temp[y] = [];

        for (let x = 0; x < CANVAS_WIDTH / PIXEL_SIZE; x++) {
            temp[y][x] = "#FFF";
        }
    }
    return temp;
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}
