FROM node:12

# Create server directory
WORKDIR /usr/src/server

# Install server dependencies
COPY package*.json ./

RUN npm install
# If you are building you code for production
# RUN npm ci --only=production

# Bundle server source
COPY . .

EXPOSE 8080

CMD [ "node", "server.js" ]