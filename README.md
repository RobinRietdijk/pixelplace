# Pixel Place

##Build Setup
Make sure [node.js](https://nodejs.org/en/) is installed.

* Install dependencies \
`npm install`
* Run dev \
`npm run start`
* Navigate to localhost:3000