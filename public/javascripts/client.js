$(document).ready(() => {
    let socket = io()

    let canvas = $("#canvas");
    let ctx = canvas[0].getContext("2d")

    let coolDown = 0;
    let imageData;

    let mouseX;
    let mouseY;
    let cancelHover = false;
    const isAdmin = window.location.pathname === "/admin"
    let gameStatus = true;

    let COOLDOWN_DURATION = 5;
    const PIXEL_SIZE = 10,
        CANVAS_BORDER_SIZE = 2;

    let lastDrawn = {x: 0, y: 0};
    let mouseDown = false;

    // Update the canvas when the entire canvas is changed
    socket.on("update_canvas", data => {
        console.log('receive canvas')
        data.forEach((row, rowIndex) => {
            row.forEach((col, colIndex) => {
                ctx.fillStyle = col
                ctx.fillRect(
                    colIndex * PIXEL_SIZE,
                    rowIndex * PIXEL_SIZE,
                    PIXEL_SIZE,
                    PIXEL_SIZE)
            })
        })
        // Save the canvas data locally
        imageData = ctx.getImageData(0, 0, canvas.width(), canvas.height());
    })

    // Update the canvas when a user places a pixel
    socket.on("update_pixel", data => {
        const rgb = hexToRgb(data.color);
        const px = data.x * PIXEL_SIZE;
        const py = data.y * PIXEL_SIZE;
        const r = rgb.r;
        const g = rgb.g;
        const b = rgb.b;
        const a = 255;

        for (let i = py; i < py + PIXEL_SIZE; i++) {
            for (let j = px; j < px + PIXEL_SIZE; j++) {
                // Update the imageData manually to prevent the placeholder pixel from being saved onto the canvas
                const index = (i * (imageData.width * 4)) + (j * 4);
                imageData.data[index] = r;
                imageData.data[index+1] = g;
                imageData.data[index+2] = b;
                imageData.data[index+3] = a;
            }
         }
    })

    // Update the online counter
    socket.on('online_counter', data => {
        $('#online-text').html(data.count);
    })

    // Update the cooldown
    socket.on('cooldown', data => {
        COOLDOWN_DURATION = data.cooldown;
    })

    // Update the game status
    socket.on('game_status', data => {
        gameStatus = data.status;
        const coolDownBox = $('#cooldown')

        if (!data.status) {
            coolDown = 0;
            coolDownBox.css({display: "block"})
            coolDownBox.html('PAUSED');
        } else {
            if (coolDownBox.html() === "PAUSED") {
                coolDownBox.css({display: "none"})
            }
        }
    })

    canvas.mousedown(() => {
        if (!isAdmin) return;
        mouseDown = true;

        adminDraw();
    })

    function adminDraw() {
        if (mouseX !== lastDrawn.x || mouseY !== lastDrawn.y) {
            const color = rgbToHex($("#palette span.selected")[0].style.backgroundColor)
            socket.emit("place_pixel", {
                x: mouseX,
                y: mouseY,
                color: color
            });
            lastDrawn.x = mouseX;
            lastDrawn.y = mouseY;
        }

        if (mouseDown) setTimeout(adminDraw, 1);
    }

    canvas.mouseup(() => {
        if (!isAdmin) return;
        mouseDown = false;
    })

    // Onclick event to place a pixel
    canvas.click((e) => {
        if (isAdmin) {
        } else {

            const coolDownBox = $('#cooldown');
            if (coolDown > 0 || !gameStatus) return;

            const mx = e.pageX;
            const my = e.pageY;
            const color = rgbToHex($("#palette span.selected")[0].style.backgroundColor)

            // Send a request to place a pixel
            socket.emit("place_pixel", {
                x: Math.floor((mx - canvas.offset().left - CANVAS_BORDER_SIZE) / PIXEL_SIZE),
                y: Math.floor((my - canvas.offset().top - CANVAS_BORDER_SIZE) / PIXEL_SIZE),
                color: color
            });

            if (COOLDOWN_DURATION < 1) return;

            // Set the cooldown
            coolDown = COOLDOWN_DURATION;
            coolDownBox.css({display: "block"})
            coolDownBox.html(coolDown);

            // Set an interval to update every second
            const x = setInterval(() => {
                // Update the cooldown every cycle (1 sec)
                if (!gameStatus) {
                    clearInterval(x);
                    return;
                }

                coolDown--;
                coolDownBox.html(coolDown);

                if (coolDown <= 0) {
                    coolDownBox.css({display: "none"})
                    clearInterval(x);
                }
            }, 1000)
        }
    })

    // Update the mouse location
    canvas.mousemove((e) => {
        mouseX = Math.floor((e.pageX - canvas.offset().left - CANVAS_BORDER_SIZE) / PIXEL_SIZE);
        mouseY = Math.floor((e.pageY - canvas.offset().top - CANVAS_BORDER_SIZE) / PIXEL_SIZE);
    });

    /**
     * Function to gather the required variables for the render cycle.
     */
    function initRender() {
        const canvasHover = cancelHover ? false : $('#canvas:hover').length !== 0;
        const coordinatesText = $('#coordinate-text');
        const color = rgbToHex($("#palette span.selected")[0].style.backgroundColor);
        render(ctx, canvasHover, gameStatus, mouseX, mouseY, coolDown, imageData, coordinatesText, PIXEL_SIZE, color);
    }

    // Set the render cycle
    setInterval(initRender, 50);
})