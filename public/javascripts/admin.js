$(document).ready(() => {
    let socket = io();
    const adminCode = $("#admin-code");
    const adminError = $("#admin-error");
    adminError.html("");
    adminError.css("visibility", "hidden");

    // Check if admin password is correct.
    $("#create").click(function () {
        const code = adminCode.val().replace(/</g, "&lt;").replace(/>/g, "&gt;");
        socket.emit("admin-code", {code: code});
    });

    // when a valid admin room code is entered, go to that room page
    // otherwise tell user that the admin room does not exist
    socket.on("admin-check", data => {
        if (data.check) {
            $("#authenticate").css("display", "none");
            $("#app").css("display", "block");
        } else {
            adminError.html("Wachtwoord is incorrect");
            adminError.css("visibility", "visible");
            adminCode.val("");
        }
    });


    let coolDown = 5;
    let intervalTimer = 10000
    let intervalStatus = false;
    let status = true;

    socket.on('cooldown', data => {
        coolDown = data.cooldown;
        $('#currentcd').html(coolDown + 's');
    })

    socket.on('random_pixel_status', data => {
        intervalStatus = data.status;
        if (intervalStatus) {
            $('#random_pixel_status').html('STARTED');
        } else {
            $('#random_pixel_status').html('STOPPED');
        }
    });

    socket.on('random_pixel_interval', data => {
        intervalTimer = data.interval;
        $('#current_interval').html(intervalTimer + 'ms');
    });

    socket.on('game_status', data => {
        status = data.status;

        if (status) {
            $('#status').html('RESUMED');
        } else {
            $('#status').html('PAUSED');
        }
    })

    $('#cd_up').click(() => {
        socket.emit('cooldown', {cooldown: coolDown + 1});
    });

    $('#cd_down').click(() => {
        if (coolDown <= 0) return;
        socket.emit('cooldown', {cooldown: coolDown -1});
    });

    $('#start').click(() => {
        socket.emit('random_pixel_start', null);
    })

    $('#stop').click(() => {
        socket.emit('random_pixel_stop', null);
    });

    $('#submit_interval').click(() => {
        const intervalBox = $('#custom_interval');
        const text = intervalBox.val();
        if (isNaN(text)) {
            alert('You must supply a number');
            intervalBox.val('');
            return;
        }
        const number = parseInt(text);
        if (number <= 0) {
            alert('You must supply a number >= 0');
            intervalBox.val('');
            return;
        }

        if (number <= 1000) {
            if (!confirm('Are you sure you want to set the interval to ' + number + '?\n' +
                'Values under 1000 can cause lag.')) {
                return;
            }
        }

        socket.emit('random_pixel_set_interval', {interval: number});
        intervalBox.val('');
    })

    $('#pause').click(() => {
        socket.emit('update_game_status', {status: false});
    })

    $('#unpause').click(() => {
        socket.emit('update_game_status', {status: true});
    })

    $('#reset').click(() => {
        if (confirm('Are you sure you want to reset the canvas?')) {
            socket.emit('reset', null);
        }
    })
})