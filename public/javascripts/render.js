/**
 * Render the coordinates onto the coordinate box.
 * @param x coordinate to be rendered
 * @param y coordinate to be rendered
 * @param coordinatesBox The box on which to render the coordinates
 */
function renderCoordinates(x, y, coordinatesBox) {
    coordinatesBox.html('(' + x + ', ' + y + ')');
}

/**
 * Render the canvas using the saved image data.
 * @param ctx The context on which to render the image
 * @param imageData The image to be rendered
 */
function renderCanvas(ctx, imageData) {
    if (!imageData) return; //????
    ctx.putImageData(imageData, 0, 0);
}

/**
 * Render the placeholder pixel under the users cursor.
 * @param ctx The context on which to render
 * @param x The x coordinate of the cursor
 * @param y The y coordinate of the cursor
 * @param PIXEL_SIZE The pixel size of this canvas
 * @param color The color to be rendered
 */
function renderPlaceholder(ctx, x, y, PIXEL_SIZE, color) {
    ctx.save();
    ctx.fillStyle = '#000'
    ctx.fillRect(
        x * PIXEL_SIZE,
        y * PIXEL_SIZE,
        PIXEL_SIZE,
        PIXEL_SIZE
    );
    ctx.fillStyle = color;
    ctx.fillRect(
        x * PIXEL_SIZE,
        y * PIXEL_SIZE,
        PIXEL_SIZE,
        PIXEL_SIZE
    );
    ctx.restore();
}

/**
 * Render cycle.
 */
function render(ctx, isHovering, gameStatus, x, y, coolDown, imageData, coordinatesBox, PIXEL_SIZE, color) {
    renderCanvas(ctx, imageData);
    if (isHovering) {
        renderCoordinates(x, y, coordinatesBox);

        if (coolDown <= 0 && gameStatus) {
            renderPlaceholder(ctx, x, y, PIXEL_SIZE, color);
        }
    }
}