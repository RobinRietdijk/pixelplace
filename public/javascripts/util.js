/**
 *  Transform an rgb color code to hex
 */
function rgbToHex(rgb) {
    let arr = rgb.slice(4, -1).split(", ")
    return "#" + ((1 << 24) + (parseInt(arr[0]) << 16) + (parseInt(arr[1]) << 8) + parseInt(arr[2])).toString(16).slice(1);
}

/**
 * Transforms a hex string to rgb color values
 */
function hexToRgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}