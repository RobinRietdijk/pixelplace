/**
 * Onclick function needed to specify what span was clicked inside the palette.
 * @param e The element that was clicked
 */
function onPaletteClick(e) {
    $('.selected')[0].className = '';
    e.className = 'selected'
}